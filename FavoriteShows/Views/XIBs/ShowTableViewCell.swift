//
//  ShowTableViewCell.swift
//  FavoriteShows
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import UIKit

class ShowTableViewCell: UITableViewCell {

    //MARK:- IBOutlets
    @IBOutlet weak var showNameLabel: UILabel!
    @IBOutlet weak var showYearLabel: UILabel!
    @IBOutlet weak var showRateLabel: UILabel!
    
    //MARK:- Cell life cycle functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        showNameLabel.text = ""
        showYearLabel.text = ""
        showRateLabel.text = ""
    }
    
    //MARK:- Cell setup
    func setupCell(WithShow show: ShowViewModel) {
        showNameLabel.text = show.name
        showYearLabel.text = show.year
        showRateLabel.text = "\(show.rate)/10"
    }
}
