//
//  FavoriteShowsTableViewController.swift
//  FavoriteShows
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import UIKit

class FavoriteShowsTableViewController: UITableViewController {

    //MARK:- properties
    let viewModel = FavoriteShowsViewModel()
    fileprivate let cellIdentifier = "ShowTableViewCell"
    private var isRandomRateActive = false

    //MARK:- View lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        loadFavoriteShows()
        setupTableView()
    }
    
    //MARK:- Data Loading
    func loadFavoriteShows() {
        viewModel.loadFavoriteShows()
    }
    
    //MARK:- UISetup
    func setupTableView() {
        tableView.register(UINib(nibName: "ShowTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.tableFooterView = UIView()
    }
    
    func showErrorMessage(withMessage errorMessage: String) {
        let alert = UIAlertController(title: "Error!", message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func showUpdateRatingMessage(forShow show: ShowViewModel) {
        let ratingAlert = UIAlertController(title: show.name, message: "Change rate", preferredStyle: .alert)
        ratingAlert.addTextField { (textField) in
            textField.placeholder = "Enter your rate"
            textField.keyboardType = .numberPad
        }
        ratingAlert.addAction(UIAlertAction(title: "Rate", style: .default, handler: { [weak ratingAlert] (_) in
            let textField = ratingAlert!.textFields![0] // Force unwrapping because we are sure it exists.
            self.validateUserRating(forNewRating: Int(textField.text ?? "\(show.rate)")!, showId: show.id)
        }))
        ratingAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(ratingAlert, animated: true, completion: nil)
    }
    
    func validateUserRating(forNewRating rating: Int, showId: String) {
        if viewModel.isValidRating(rating) {
            viewModel.updateRating(forShowId: showId, newRate: rating)
        } else {
            showErrorMessage(withMessage: "New rate cannot be more than 10")
        }
    }
    
    //MARK:- Actions
    @IBAction func randomRatePressed(_ sender: UIBarButtonItem) {
        isRandomRateActive = !isRandomRateActive
        var timer = Timer()
        if isRandomRateActive {
            timer = Timer.scheduledTimer(withTimeInterval: CDouble(arc4random_uniform(6)), repeats: true, block: { _ in
                self.viewModel.randomlyChangeRating()
            })
            sender.title = "Stop"
        } else {
            timer.invalidate()
            sender.title = "Random Rating"
        }
    }
}

//MARK:- View model delegate functions
extension FavoriteShowsTableViewController: FavoriteShowsViewModelDelegate {
    func didLoadShows(_ favoriteShows: [ShowViewModel]) {
        tableView.reloadData()
    }
    
    func failedToLoadShows(_ error: LoadingDataError) {
        showErrorMessage(withMessage: error.localizedDescription)
    }
}

//MARK:- Table view data source and delegate
extension FavoriteShowsTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.favoriteShows.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ShowTableViewCell
        cell.setupCell(WithShow: viewModel.favoriteShows[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showUpdateRatingMessage(forShow: viewModel.favoriteShows[indexPath.row])
    }
}
