//
//  FavoriteShowsViewModel.swift
//  FavoriteShows
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import Foundation

protocol FavoriteShowsViewModelDelegate: class {
    func didLoadShows(_ favoriteShows: [ShowViewModel])
    func failedToLoadShows(_ error: LoadingDataError)
}

class FavoriteShowsViewModel {
    //MARK:- Properties
    var favoriteShows: [ShowViewModel] = []
    weak var delegate: FavoriteShowsViewModelDelegate?
    
    //MARK:- Loading data
    func loadFavoriteShows() {
        DataManager.loadShows { result in
            switch result {
            case .success(let responseShows):
                self.favoriteShows = responseShows.shows.map(ShowViewModel.init)
                self.sortShowsByRate(self.favoriteShows)
            case .failure(let error):
                self.delegate?.failedToLoadShows(error)
            }
        }
    }
    
    //MARK:- Sort shows
    private func sortShowsByRate(_ shows: [ShowViewModel]){
        favoriteShows = shows.sorted(by: { $0.rate > $1.rate })
        delegate?.didLoadShows(favoriteShows)
    }
    
    //MARK:- Update rating for show
    func updateRating(forShowId showID: String, newRate: Int) {
        favoriteShows.filter({$0.id == showID}).first?.rate = newRate
        sortShowsByRate(favoriteShows)
    }
    
    func randomlyChangeRating() {
        let randomShow = favoriteShows.randomElement()
        updateRating(forShowId: randomShow!.id, newRate: Int.random(in: 0 ... 10))
    }
    
    func isValidRating(_ rating: Int) -> Bool {
        if rating <= 10 {
            return true
        }
        return false
    }
}
