//
//  ShowViewModel.swift
//  FavoriteShows
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import Foundation

class ShowViewModel {
    //MARK:- Properties
    var show: Show
    
    var id: String {
        return show.id
    }
    
    var name: String {
        return show.name
    }
    var year: String {
        return show.year
    }
    var rate: Int {
        get {
            return show.rate
        } set {
            updateRate(newValue)
        }
    }
    
    //MARK:- Initializers
    init(with show: Show) {
        self.show = show
    }
    
    //MARK:- Updating rate
    private func updateRate(_ newValue: Int) {
        show.rate = newValue
    }
}
