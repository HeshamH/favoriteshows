//
//  DataManager.swift
//  FavoriteShows
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import Foundation

/// Data manager is responsible for loading and fetching data
class DataManager {
    
    enum LoadingResult<Value> {
        case success(Value)
        case failure(LoadingDataError)
    }
    
    /// Loading data from JSON file
    /// - Parameter fileName: JSON File Name
    /// - Parameter completion: completion is escaping call back closure that returns a generic codable model or an error in case of error
    private static func loadDataFromFile<T: Decodable>(_ fileName: String, completion: @escaping (LoadingResult<T>)-> Void) {
        guard let path = Bundle.main.path(forResource: fileName, ofType: Config.jsonFileType) else {
            completion(.failure(.parsingError("JSON file not exist")))
            return
        }
        do {
            let loadedData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            do {
                let response = try JSONDecoder().decode(T.self, from: loadedData)
                completion(.success(response))
            } catch {
                completion(.failure(.parsingError("Failed to parse data from the json")))
            }
        } catch {
             completion(.failure(.parsingError("Failed to load data from the file")))
        }
    }
    
    
    /// Loading favorite shows from json file
    /// - Parameter completion: completion is escaping call back closure that returns a codable model from the loaded json
    static func loadShows(completion: @escaping (LoadingResult<FavoriteShows>) -> Void) {
        loadDataFromFile(Config.showsFileResourceName, completion: completion)
    }
}
