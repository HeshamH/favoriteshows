//
//  FavoriteShows.swift
//  FavoriteShows
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import Foundation

struct FavoriteShows: Codable {
    let shows: [Show]
}

struct Show: Codable {
    let id: String
    let name: String
    let year: String
    var rate: Int
}

extension Show {
    enum CodingKeys: String, CodingKey {
        case id = "showId"
        case name, year, rate
    }
}
