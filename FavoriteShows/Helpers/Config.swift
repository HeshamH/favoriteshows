//
//  Config.swift
//  FavoriteShows
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import Foundation

struct Config {
    static let showsFileResourceName = "Shows"
    static let jsonFileType = "json"
}
