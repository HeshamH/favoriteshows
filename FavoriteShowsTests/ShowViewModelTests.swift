//
//  ShowViewModelTests.swift
//  FavoriteShowsTests
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import XCTest
@testable import FavoriteShows

class ShowViewModelTests: XCTestCase {
    
    var viewModelUnderTest: ShowViewModel?
    
    override func setUp() {
        let show = Show(id: "1", name: "Gotham", year: "2006", rate: 5)
        viewModelUnderTest = ShowViewModel(with: show)
    }

    override func tearDown() { }
    
    func testViewModelData() {
        XCTAssertNotNil(viewModelUnderTest?.id, "Failed to parse id to view model")
        XCTAssertNotNil(viewModelUnderTest?.name, "Failed to parse name to view model")
        XCTAssertNotNil(viewModelUnderTest?.year, "Failed to parse year to view model")
        XCTAssertNotNil(viewModelUnderTest?.rate, "Failed to parse rate to view model")
        XCTAssertEqual(viewModelUnderTest?.id, "1", "Wrong show id assigned")
        XCTAssertEqual(viewModelUnderTest?.name, "Gotham", "Wrong show name assigned")
        XCTAssertEqual(viewModelUnderTest?.year, "2006", "Wrong show year assigned")
        XCTAssertEqual(viewModelUnderTest?.rate, 5, "Wrong show rate assigned")
    }
}
