//
//  ShowModelTests.swift
//  FavoriteShowsTests
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import XCTest
@testable import FavoriteShows

class ShowModelTests: XCTestCase {

    var showUnderTest: Show?
    
    override func setUp() {
        let testJson = ["showId": "1",
                        "name": "Breaking Bad",
                        "year": "2008",
                        "rate": 9] as [String : Any]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: testJson, options: .prettyPrinted)
            do {
                showUnderTest = try JSONDecoder().decode(Show.self, from: jsonData)
            } catch {
                XCTAssertThrowsError(error.localizedDescription)
            }
        } catch {
            XCTAssertThrowsError(error.localizedDescription)
        }
    }

    override func tearDown() { }
    
    func testModelData() {
        XCTAssertNotNil(showUnderTest?.id, "Failed to parse id to show model")
        XCTAssertNotNil(showUnderTest?.name, "Failed to parse name to show model")
        XCTAssertNotNil(showUnderTest?.year, "Failed to parse year to show model")
        XCTAssertNotNil(showUnderTest?.rate, "Failed to parse rate to show model")
        XCTAssertEqual(showUnderTest?.id, "1", "Wrong show id assigned")
        XCTAssertEqual(showUnderTest?.name, "Breaking Bad", "Wrong show name assigned")
        XCTAssertEqual(showUnderTest?.year, "2008", "Wrong show year assigned")
        XCTAssertEqual(showUnderTest?.rate, 9, "Wrong show rate assigned")
    }
    
    
}
