//
//  FavoriteShowsViewModelDelegateMock.swift
//  FavoriteShowsTests
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import Foundation
import XCTest
@testable import FavoriteShows

/// This class can be used to mock the delegate of the FavoriteShowsViewModel
class FavoriteShowsViewModelDelegateMock: FavoriteShowsViewModelDelegate {
    var showsViewModels: [ShowViewModel]?
    var error: LoadingDataError?
    
    var expectation: XCTestExpectation?
    
    func didLoadShows(_ favoriteShows: [ShowViewModel]) {
        showsViewModels = favoriteShows
        expectation?.fulfill()
    }
    
    func failedToLoadShows(_ error: LoadingDataError) {
        self.error = error
        expectation?.fulfill()
    }
}
