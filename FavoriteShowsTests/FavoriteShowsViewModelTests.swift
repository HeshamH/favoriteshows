//
//  FavoriteShowsViewModelTests.swift
//  FavoriteShowsTests
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import XCTest
@testable import FavoriteShows

class FavoriteShowsViewModelTests: XCTestCase {

    var viewModelUnderTest: FavoriteShowsViewModel?
    let viewModelUnderTestDelegate = FavoriteShowsViewModelDelegateMock()
    
    override func setUp() {
        viewModelUnderTest = FavoriteShowsViewModel()
        let testShows: [Show] = [Show(id: "1", name: "Show 1", year: "2010", rate: 3),
                                 Show(id: "2", name: "Show 2", year: "2008", rate: 4),
                                 Show(id: "3", name: "Show 3", year: "2006", rate: 1)]
        let fakeShowsViewModels: [ShowViewModel] = [ShowViewModel(with: testShows[0]), ShowViewModel(with: testShows[1]), ShowViewModel(with: testShows[2])]
        viewModelUnderTest?.favoriteShows = fakeShowsViewModels
    }

    override func tearDown() { }
    
    func testLoadingFavoriteQuestions() {
        let exp = expectation(description: "Func calls the delegate as the result of an async method completion")
        viewModelUnderTest?.delegate = viewModelUnderTestDelegate
        viewModelUnderTestDelegate.expectation = exp
        viewModelUnderTest?.loadFavoriteShows()
        
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            guard let favoriteShows = self.viewModelUnderTestDelegate.showsViewModels else {
                guard let error = self.viewModelUnderTestDelegate.error else {
                    XCTFail("Expected delegate to be called")
                    return
                }
                XCTAssertNotNil(error)
                return
            }
            XCTAssertNotNil(favoriteShows)
        }
    }
    
    func testRatingUpdate() {
        let expectedRate = 7
        viewModelUnderTest?.updateRating(forShowId: (viewModelUnderTest?.favoriteShows[0].id)!, newRate: 7)
        XCTAssertEqual(viewModelUnderTest?.favoriteShows[0].rate, expectedRate, "Wrong rate update")
    }
    
    func testRandomlyChangeRate() {
        let expectedRate = Int.random(in: 0 ... 10)
        let randomShow = viewModelUnderTest?.favoriteShows.randomElement()
        viewModelUnderTest?.updateRating(forShowId: randomShow!.id, newRate: expectedRate)
        XCTAssertEqual(randomShow!.rate, expectedRate, "Wrong rate update")
    }
    
    func testValidatingRate() {
        XCTAssertFalse((viewModelUnderTest?.isValidRating(14))!)
    }
}
