//
//  FavoriteShowsModelTests.swift
//  FavoriteShowsTests
//
//  Created by Hesham on 11/18/19.
//  Copyright © 2019 Hesham. All rights reserved.
//

import XCTest
@testable import FavoriteShows

class FavoriteShowsModelTests: XCTestCase {
    
    var favoriteShowsUnderTest: FavoriteShows?
    
    override func setUp() {
        let testJson = ["shows": [["showId": "1",
                                   "name": "Breaking Bad",
                                   "year": "2008",
                                   "rate": 9 ],
                                  ["showId": "2",
                                   "name": "Game of Thrones",
                                   "year": "2011",
                                   "rate": 8]]]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: testJson, options: .prettyPrinted)
            do {
                favoriteShowsUnderTest = try JSONDecoder().decode(FavoriteShows.self, from: jsonData)
            } catch {
                XCTAssertThrowsError(error.localizedDescription)
            }
        } catch {
            XCTAssertThrowsError(error.localizedDescription)
        }
    }

    override func tearDown() { }
    
    func testDataExistance() {
        XCTAssertNotNil(favoriteShowsUnderTest, "Favorite shows is nil")
        XCTAssertEqual(favoriteShowsUnderTest?.shows.count, 2, "Favorite shows count is not correct")
    }
}
